const express = require('express');
const router = express.Router();
const config = require('../config/database');

const Lesson = require('./../models/lesson');


//get list of lessons from db
router.get('/lessons', function (req, res, next) {
  Lesson.find({}).then(function (lessons) {
    res.send(lessons);
  });
});


// Pobranie jednego elementu na podstawie id
router.get('/lessons/:id', function (req, res, next) {
  Lesson.findOne({
    _id: req.params.id
  }).then(function (lesson) {
    res.send(lesson);
  });
});

//add new lesson to db
router.post('/lessons', function (req, res, next) {

  Lesson.create(req.body).then(function (lesson) {
    res.send(lesson);
  }).catch(next);

});

// update a lesson in the db
router.put('/lessons/:id', function (req, res, next) {
  Lesson.findByIdAndUpdate({
    _id: req.params.id
  }, req.body).then(function (lesson) {
    Lesson.findOne({
      _id: req.params.id
    }).then(function (lesson) {
      res.send(lesson);
    });
  });
});

// delete a lesson in the db
router.delete('/lessons/:id', function (req, res, next) {
  Lesson.findByIdAndRemove({
    _id: req.params.id
  }).then(function (lesson) {
    res.send(lesson);
  });
});

module.exports = router;
