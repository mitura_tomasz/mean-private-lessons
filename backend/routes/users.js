const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');

const User = require('../models/user');

// Register
router.post('/register', (req, res, next) => {
  let newUser = new User({
    name: req.body.name,
    surname: req.body.surname,
    username: req.body.username,
    email: req.body.email,
    phone: req.body.phone,
    password: req.body.password,
    lessonsID: req.body.lessonsID
  });

  User.addUser(newUser, (err, user) => {
    if (err) {
      res.json({
        success: false,
        msg: 'Failed to register user'
      });
    } else {
      res.json({
        success: true,
        msg: 'User registered'
      });
    }
  });
});

// Authenticate
router.post('/authenticate', (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;

  User.getUserByUsername(username, (err, user) => {
    if (err) throw err;
    if (!user) {
      return res.json({
        success: false,
        msg: 'User not found'
      });
    }

    User.comparePassword(password, user.password, (err, isMatch) => {
      if (err) throw err;
      if (isMatch) {
        const token = jwt.sign(user, config.secret, {
          expiresIn: 604800 // 1 week 
        });

        res.json({
          success: true,
          token: 'JWT ' + token,
          user: {
            id: user._id,
            name: user.name,
            username: user.username,
            email: user.email
          }
        });
      } else {
        return res.json({
          success: false,
          msg: "Wrong password"
        });
      }
    });
  });
});

// Profile
router.get('/profile', passport.authenticate('jwt', {
  session: false
}), (req, res, next) => {
  res.json({
    user: req.user
  });
});

// Profile-id
router.get('/profile/:id', (req, res, next) => {
  User.findById({
    _id: req.params.id
  }, req.body).then(function (user) {
    User.findOne({
      _id: req.params.id
    }).then(function (user) {
      res.send(user);
    });
  });

});

// dodanie id nowych korepetycji do user.privateLessonID[]
router.put('/addLessonID/:id', (req, res, next) => {
  User.findByIdAndUpdate({
    _id: req.params.id
  }, req.body).then(function (user) {
    User.findOne({
      _id: req.params.id
    }).then(function (user) {
      res.send(user);
    });
  });
});

// usuwanie id nowych korepetycji do user.privateLessonID[]
router.put('/removeLessonID/:id', (req, res, next) => {
  User.findByIdAndUpdate({
    _id: req.params.id
  }, req.body).then(function (user) {
    User.findOne({
      _id: req.params.id
    }).then(function (user) {
      res.send(user);
    });
  });
});


module.exports = router;
