const express = require('express');
const router = express.Router();
const nodemailer = require('nodemailer');

router.post('/send', (req, res) => {

  const output = `
    <p>Dostales wiadomosc od użytkownika ${req.body.senderUsername} o treści: </p>
    <p>
      ${req.body.message}
    </p>
    <p>Aby odpowiedzieć wyślij wiadomość na mail użytkownika ${req.body.senderUsername} o adresie ${req.body.senderEmailAddress} </p>
  `;

  // Generate test SMTP service account from ethereal.email
  nodemailer.createTestAccount((err, account) => {

    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
      host: 'smtp.gmail.com',
      port: 587,
      secure: false, // true for 465, false for other ports
      auth: {
        user: 'mitur5g@gmail.com', // generated ethereal user
        pass: 'plny1234' // generated ethereal password
      },
      tls: {
        rejectUnauthorized: false
      }
    });

    // setup email data with unicode symbols
    let mailOptions = {
      from: '"Centrum Korepetycji" <putin@test@traversymedia.com>', // sender address
      to: req.body.receiverEmailAddress, // list of receivers
      subject: 'Nowa_wiadomość.', // Subject line
      html: output
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        return console.log(error);
      }
      console.log('Message sent: %s', info.messageId);
      console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
      res.render('contact', {
        msg: 'Email has been sent'
      });
    });
  });

});


module.exports = router;
