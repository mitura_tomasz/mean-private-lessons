const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

// User Lesson
const LessonSchema = mongoose.Schema({
  title: {
    type: String
  },
  subject: String,
  pricePerHour: Number,
  town: String,
  description: String,
  selectedPlaceOfLesson: String,
  levels: [String],
  placeOfLesson: String,
  ownerID: {
    type: mongoose.Schema.Types.ObjectId
  }

});

const Lesson = module.exports = mongoose.model('Lesson', LessonSchema);
