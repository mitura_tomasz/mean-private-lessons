const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const mongoose = require('mongoose');
const nodemailer = require('nodemailer');
const config = require('./config/database');

// Connect to database
mongoose.connection.openUri(config.database);

// On connection
mongoose.connection.on('connected', () => {
  console.log('Connected to database ' + config.database);
});

// On error
mongoose.connection.on('error', (err) => {
  console.log('Database error ' + err);
});


const app = express();

const users = require('./routes/users');
const lessons = require('./routes/lessons');
const mail = require('./routes/mail');

const port = 3000;

// CORS middleware
app.use(cors());

// Set static folder
app.use(express.static("./../frontend/dist"));

// Body parser middleware
app.use(bodyParser.json());

// Passport middleware
app.use(passport.initialize());
app.use(passport.session());

require('./config/passport')(passport);

app.use('/users', users);
app.use('/api', lessons);
app.use(mail);

// Index route
app.get('/', (req, res) => {
  res.send('Invalid Endpoint');
});

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'public/index.html'));
});


// Start server
app.listen(port, () => {
  console.log('Server started on port ' + port);
});
