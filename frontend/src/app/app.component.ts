import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

import { PrivateLessonsService } from './services/private-lessons.service';

import { PrivateLesson } from './classes/private-lesson';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  constructor(
    private _privateLessonsService: PrivateLessonsService,
    private _router: Router
  ) { }
  
  ngOnInit() {
    this._privateLessonsService.getPrivateLessons().subscribe(
      responseData => {
        this._privateLessonsService.setFilteredPrivateLessons(responseData);
      }
    );
  }
  
  onActivate(event: Event) {
    window.scrollTo(0, 0);
  }
  
}
