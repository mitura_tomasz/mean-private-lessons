import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ValidateService } from '../../services/validate.service';
import { AuthService } from '../../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  username: String;
  email: String;
  password: String;

  constructor(
    private _validateService: ValidateService,
    private _flashMessage: FlashMessagesService,
    private _authService: AuthService,
    private _router: Router
  ) { }

  ngOnInit() { }

  onRegisterSubmit() {
    const user = {
      name: '',
      surname: '',
      username: this.username,
      email: this.email,
      phone: "",
      password: this.password,
      lessonsID: [],
      favoriteLessonsID: []

    }

    // Required Fields
    if (!this._validateService.validateRegister(user)) {
      this._flashMessage.show(
        'Proszę wypełnić wszystkie pola',
        { cssClass: 'alert-danger', timeout: 3000 }
      );
      return false;
    }

    // Validate Email
    if (!this._validateService.validateEmail(user.email)) {
      this._flashMessage.show(
        'Proszę użyc prawidłowego adresu e-mail',
        { cssClass: 'alert-danger', timeout: 3000 }
      );
      return false;
    }

    // Register user
    this._authService.registerUser(user).subscribe(data => {
      if (data.success) {
        this._flashMessage.show(
          'Zostałeś zarejestrowany i możesz sie teraz zalogować',
          { cssClass: 'alert-success', timeout: 3000 }
        );
        this._router.navigate(['/login']);
      } else {
        this._flashMessage.show(
          'Coś poszło nie tak',
          { cssClass: 'alert-danger', timeout: 3000 }
        );
        this._router.navigate(['/register']);
      }
    });

  }
}
