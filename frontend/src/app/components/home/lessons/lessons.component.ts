import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Lessons } from '../../../classes/lessons';
import { PrivateLesson } from './../../../classes/private-lesson';

import { LessonsService } from '../../../services/lessons.service';
import { PrivateLessonsService } from './../../../services/private-lessons.service';
import { CitiesService } from './../../../services/cities.service';


@Component({
  selector: 'app-lessons',
  templateUrl: './lessons.component.html',
  styleUrls: ['./lessons.component.scss'],
  providers: [LessonsService],
})

export class LessonsComponent implements OnInit {

  lessons: Lessons[];
  privateLessons: PrivateLesson[];

  constructor(
    private _lessonsService: LessonsService,
    private _router: Router,
    private _privateLessonsService: PrivateLessonsService,
  ) { }

  ngOnInit() {
    this.lessons = this._lessonsService.getLessons();
    this._privateLessonsService.getPrivateLessons().subscribe(
      responsePrivateLessons => this.privateLessons = responsePrivateLessons
    );
  }


  onLessonClick(lesson: string) {

    this._router.navigate(['announcements']).then(() => {
      let form = {
        "town": null,
        "subject": lesson,
        "level": null,
        "priceMin": null,
        "priceMax": null
      };

      let searchResults = this._privateLessonsService.getSearchResults(["subject"], form);
      this._privateLessonsService.setFilteredPrivateLessons(searchResults);
    });

  }

  getNumberOfAnnoucements(subject: string) {
    if (this.privateLessons) {
      let count = 0;
      this.privateLessons.filter(
        x => {
          if (x.subject === subject) count++;
        }
      );
      return count;
    }
  }

}
