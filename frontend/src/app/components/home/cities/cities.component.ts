import { Component, OnInit } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';

import { Cities } from '../../../classes/cities';
import { PrivateLesson } from './../../../classes/private-lesson';

import { CitiesService } from '../../../services/cities.service';
import { PrivateLessonsService } from './../../../services/private-lessons.service';


@Component({
  selector: 'app-cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.scss'],
  providers: [CitiesService],
})
export class CitiesComponent implements OnInit {

  cities: Cities[];
  selectedVoivodeship: String;
  selectedCities: Cities;
  privateLessons: PrivateLesson[];


  constructor(
    private _citiesService: CitiesService,
    private _privateLessonsService: PrivateLessonsService,
    private _router: Router

  ) { }

  ngOnInit() {
    this.cities = this._citiesService.getCities();
    this.cities.map(x => {
      x.towns = x.towns.sort();
    });

    this.selectedCities = this.cities[0];

    this.selectedVoivodeship = this.cities[0].voivodeship;

    this._privateLessonsService.getPrivateLessons().subscribe(
      responsePrivateLessons => this.privateLessons = responsePrivateLessons
    );

  }


  onSelectedCities(selectedVoivodeship) {

    selectedVoivodeship = selectedVoivodeship.substr(selectedVoivodeship.indexOf(' ') + 1);

    this.cities.filter((item) => {
      if (item.voivodeship === selectedVoivodeship) {
        this.selectedCities = item;
      }
    });

  }

  onTownClick(town) {
    this._router.navigate(['announcements']).then(() => {
      let form = {
        "town": town,
        "subject": null,
        "level": null,
        "priceMin": null,
        "priceMax": null
      };


      let searchResults = this._privateLessonsService.getSearchResults(["town"], form);
      this._privateLessonsService.setFilteredPrivateLessons(searchResults);
    });
  }

  getNumberOfAnnoucements(town: string) {
    if (this.privateLessons) {
      let count = 0;
      this.privateLessons.filter(
        x => {
          if (x.town === town) count++;
        }
      );
      return count;
    }
  }

}
