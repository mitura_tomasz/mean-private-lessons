import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  FormControl,
  FormGroup,
  FormArray,
  FormBuilder,
  Validators,
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';

import { PrivateLessonsService } from './../../services/private-lessons.service';
import { LessonsService } from './../../services/lessons.service';
import { CitiesService } from './../../services/cities.service';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  searchForm: FormGroup;
  lessons: string[];
  towns: string[]

  constructor(
    private _formBuilder: FormBuilder,
    private _privateLessonsService: PrivateLessonsService,
    private _router: Router,
    private _lessonsService: LessonsService,
    private _citiesService: CitiesService
  ) { }

  ngOnInit() {
    this.searchForm = this._formBuilder.group({
      'searchTerm': [
        null,
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(50)
        ])
      ],
      'town': null,
      'subject': null
    });

    this.lessons = this._lessonsService.getLessonsList();
    this.towns = this._citiesService.getAllTowns();
  }


  onSearchSubmit(searchForm, town, subject) {

    let searchFilters = [];

    if (searchForm.searchTerm !== null) { searchFilters.push("searchTerm"); }
    if (searchForm.subject !== null) { searchFilters.push("subject"); }
    if (searchForm.town !== null) { searchFilters.push("town"); }

    this._router.navigate(['announcements']).then(() => {
      let searchResults = this._privateLessonsService.getSearchResults(searchFilters, searchForm);
      this._privateLessonsService.setFilteredPrivateLessons(searchResults);
    });
  }

  onSelectedTownChange(town) {
    (<FormControl>this.searchForm.controls['town']).setValue(town);
  }
  onSelectedSubjectChange(subject) {
    (<FormControl>this.searchForm.controls['subject']).setValue(subject);
  }

}
