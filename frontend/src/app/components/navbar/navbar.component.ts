import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(
    private _flashMessage: FlashMessagesService,
    private _authService: AuthService,
    private _router: Router
  ) { }

  ngOnInit() { }

  onLogoutClick() {
    this._authService.logout();
    this._flashMessage.show(
      'Zostałeś wylogowany',
      {
        cssClass: 'alert-success',
        timeout: 3000,
        'margin-top': "25px"
      }
    );
    this._router.navigate(['/login']);
    return false;
  }

}
