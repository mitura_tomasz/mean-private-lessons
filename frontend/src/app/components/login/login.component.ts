import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

import { AuthService } from '../../services/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: String;
  password: String;


  constructor(
    private _flashMessage: FlashMessagesService,
    private _authService: AuthService,
    private _router: Router
  ) { }

  ngOnInit() { }

  onLoginSubmit() {
    const user = {
      username: this.username,
      password: this.password
    }

    this._authService.authenticateUser(user).subscribe(data => {
      if (data.success) {
        this._authService.storeUserData(data.token, data.user);
        this._flashMessage.show(
          "Zostałeś zalogowany pomyślnie",
          { cssClass: 'alert-success', timeout: 5000 },
        );
        this._router.navigate(['profile/account']);
      } else {
        this._flashMessage.show(
          "Podałeś niepoprawne dane",
          { cssClass: 'alert-danger', timeout: 5000 },
        );
        this._router.navigate(['login']);
      }
    });
  }

}

