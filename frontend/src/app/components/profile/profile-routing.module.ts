import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './../../guards/auth.guard';

import { ProfileComponent } from './profile.component';
import { AccountComponent } from './components/account/account.component';
import { AddOfferComponent } from './components/add-offer/add-offer.component';
import { FavoriteOffersComponent } from './components/favorite-offers/favorite-offers.component';
import { MyOffersComponent } from './components/my-offers/my-offers.component';
import { EditOfferComponent } from './components/my-offers/components/edit-offer/edit-offer.component';
import { SuccessComponent } from './components/success/success.component';


const routes: Routes = [
  {
    path: 'profile', component: ProfileComponent, canActivate: [AuthGuard], children: [
      { path: '', redirectTo: 'account', pathMatch: 'full' },
      { path: 'account', component: AccountComponent },
      { path: 'add-offer', component: AddOfferComponent },
      { path: 'favorite-offers', component: FavoriteOffersComponent },
      { path: 'my-offers', component: MyOffersComponent },
      { path: 'my-offers/edit/:id', component: EditOfferComponent },
      { path: 'success/:id', component: SuccessComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
