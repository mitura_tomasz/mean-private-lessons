import { Component, OnInit, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AuthService } from '../../services/auth.service';

import { User } from '../../classes/user';

import { AccountComponent } from './components/account/account.component';
import { AddOfferComponent } from './components/add-offer/add-offer.component';
import { FavoriteOffersComponent } from './components/favorite-offers/favorite-offers.component';
import { MyOffersComponent } from './components/my-offers/my-offers.component';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {

  user: User;
  sections: SectionInterface[] = [
    { value: 'Dodaj ogłoszenie', route: 'add-offer' },
    { value: 'Moje ogłoszenia', route: 'my-offers' },
    { value: 'Moje konto', route: 'account' },
    { value: 'Ulubione ogłoszenia', route: 'favorite-offers' },
  ];
  name: any;

  constructor(
    private _authService: AuthService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _elementRef: ElementRef
  ) { }

  ngOnInit() {
    this._authService.getProfile().subscribe(
      profile => {
        this.user = profile.user;
      },
      err => {
        console.log(err);
        return false;
      }
    );
  }

  onSectionClick(section) {

    this.sections.filter(x => {
      this._elementRef.nativeElement.querySelector('.' + x.route).classList.remove('active');
    });

    this._elementRef.nativeElement.querySelector('.' + section).classList.add('active');
  }

  getActualRoute() {
    let route;
    this.sections.filter(x => {
      if (this._router.url.includes(x.route)) {
        route = x.route;
      }
    });
    return route;
  }


  onActivate(event: Event) {
    window.scrollTo(0, 0);
  }

}

export interface SectionInterface {
  value: string;
  route: string;
}
