import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { ProfileRoutingModule } from './profile-routing.module';
import { MyOffersModule } from './components/my-offers/my-offers.module';
import { FavoriteOffersModule } from './components/favorite-offers/favorite-offers.module';
import { OfferFormModule } from './components/offer-form/offer-form.module';

import { ProfileComponent } from './profile.component';
import { AccountComponent } from './components/account/account.component';
import { AddOfferComponent } from './components/add-offer/add-offer.component';
import { SuccessComponent } from './components/success/success.component';


@NgModule({
  imports: [
    CommonModule,
    ProfileRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MyOffersModule,
    OfferFormModule,
    FavoriteOffersModule
  ],
  declarations: [
    ProfileComponent,
    AccountComponent,
    AddOfferComponent,
    SuccessComponent,
  ]
})
export class ProfileModule { }
