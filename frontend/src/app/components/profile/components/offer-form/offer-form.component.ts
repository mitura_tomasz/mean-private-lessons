import { Component, OnInit, Input } from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormArray,
  FormBuilder,
  Validators,
  AbstractControl,
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AuthService } from './../../../../services/auth.service';
import { PrivateLessonsService } from './../../../../services/private-lessons.service';
import { LessonsService } from './../../../../services/lessons.service';

import { PrivateLesson } from './../../../../classes/private-lesson';
import { User } from './../../../../classes/user';


@Component({
  selector: 'app-offer-form',
  templateUrl: './offer-form.component.html',
  styleUrls: ['./offer-form.component.scss']
})
export class OfferFormComponent implements OnInit {

  @Input() option: string;
  user: User;
  privateLesson: PrivateLesson;
  offerForm: FormGroup;
  lessonSubjects: any;

  levels: LevelInterface[];
  placeOfLesson: Object;


  constructor(
    private _formBuilder: FormBuilder,
    private _authService: AuthService,
    private _httpClient: HttpClient,
    private _activeRoute: ActivatedRoute,
    private _router: Router,
    private _privateLessonsService: PrivateLessonsService,
    private _lessonsService: LessonsService
  ) { }

  ngOnInit() {

    this.levels = [
      { level: "Szkoła podstawowa", checked: false },
      { level: "Gimnazjum", checked: false },
      { level: "Liceum", checked: false },
      { level: "Studia", checked: false }
    ];
    this.placeOfLesson = [
      { place: "U nauczyciela" },
      { place: "U ucznia" },
      { place: "U nauczyciela i ucznia" },
    ];

    this.offerForm = this._formBuilder.group({
      'title': [
        null,
        Validators.compose([
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(45)
        ])
      ],
      'subject': [null, Validators.required],
      'pricePerHour': [
        null,
        Validators.compose([
          Validators.required,
          Validators.min(0),
          Validators.max(1000)
        ])
      ],
      'town': [
        null,
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(45)
        ])
      ],
      'description': [
        null,
        Validators.compose([
          Validators.required,
          Validators.minLength(20),
          Validators.maxLength(1000)
        ])
      ],
      'placeOfLesson': [null, Validators.required]
    });

    this.lessonSubjects = this._lessonsService.getLessonSubjects();


    this._authService.getProfile().subscribe(
      profile => this.user = profile.user,
      err => {
        console.log(err);
        return false;
      }
    );


    if (this.option === 'add') {
      this.levels.filter(x => x.checked = true);
    }

    if (this.option === 'edit') {
      this._privateLessonsService.getPrivateLessonByID(this.getLessonIDFromParam()).then(
        lesson => {
          this.privateLesson = lesson;

          this.offerForm.controls['title'].setValue(this.privateLesson.title);
          this.offerForm.controls['subject'].setValue(this.privateLesson.subject);
          this.offerForm.controls['pricePerHour'].setValue(this.privateLesson.pricePerHour);
          this.offerForm.controls['town'].setValue(this.privateLesson.town);
          this.offerForm.controls['description'].setValue(this.privateLesson.description);
          this.offerForm.controls['placeOfLesson'].setValue(this.privateLesson.placeOfLesson);

          for (let i = 0; i < this.levels.length; i++) {
            for (let j = 0; j < this.privateLesson.levels.length; j++) {
              if (this.privateLesson.levels[j] === this.levels[i].level) {
                this.levels[i].checked = true;
                break;
              }
            }
          };
        }
      );
    }
  }

  onAddOfferSubmit(offerForm) {

    if (this.offerForm.valid) {
      console.log("Form Submitted!");

      this.privateLesson = new PrivateLesson(
        this.offerForm.controls['title'].value,
        this.offerForm.controls['subject'].value,
        this.offerForm.controls['pricePerHour'].value,
        this.offerForm.controls['town'].value,
        this.offerForm.controls['description'].value,
        this.offerForm.controls['placeOfLesson'].value,
        this.levelsToString(),
        JSON.parse(localStorage.getItem('user')).id
      );

      // dodanie lekcji do lessonsDB
      this._httpClient.post('http://localhost:3000/api/lessons', this.privateLesson)
      .subscribe(
        (data: any) => this._authService.addLesson(data._id),
        (err: HttpErrorResponse) => this._authService.handleErrorResponse(err)
      );
      
      this._router.navigate(['profile/success/add']);

    } else {
      console.log("Form not Submitted! Something went wrong!");
    }
  }

  onEditOfferSubmit(offerForm) {
    if (this.offerForm.valid) {
      console.log("Form Submitted!");

      this.privateLesson = new PrivateLesson(
        this.offerForm.controls['title'].value,
        this.offerForm.controls['subject'].value,
        this.offerForm.controls['pricePerHour'].value,
        this.offerForm.controls['town'].value,
        this.offerForm.controls['description'].value,
        this.offerForm.controls['placeOfLesson'].value,
        this.levelsToString(),
        JSON.parse(localStorage.getItem('user')).id
      );

      this._privateLessonsService.updatePrivateLesson(this.privateLesson, this.getLessonIDFromParam()).subscribe(
        (data: any) => console.log(data),
        (err: HttpErrorResponse) => this._authService.handleErrorResponse(err)
      );
      
      this._router.navigate(['profile/success/edit']);
    } else {
      console.log("Form not Submitted! Something went wrong!");
    }
  }

  onOfferSubmit(offerForm) {
    this.option === 'add' ? this.onAddOfferSubmit(offerForm.value) : this.onEditOfferSubmit(offerForm.value);
  }

  onLessonLevelChange(level: string) {
    let result = this.levels.filter((obj) => {
      if (obj.level === level) {
        obj.checked = !obj.checked;
      }
    });
  }

  getLessonIDFromParam(): string {
    let lessonID;
    this._activeRoute.paramMap.subscribe(params => {
      lessonID = params.get('id');
    });
    return lessonID;
  }

  levelsToString(): string[] {
    let result = [];
    this.levels.filter((obj) => {
      if (obj.checked === true) {
        result.push(obj.level);
      }
    });
    return result;
  }


  isAnyLevelSelected() {
    let result = false;
    this.levels.filter(x => { if (x.checked) result = true });
    return result;
  }
}

export interface LevelInterface {
  level: string;
  checked: boolean;
}
