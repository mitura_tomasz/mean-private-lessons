import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { OfferFormComponent } from './offer-form.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule, 
    ReactiveFormsModule,
    RouterModule
  ],
  declarations: [
    OfferFormComponent
  ],
  exports: [ OfferFormComponent ]
})
export class OfferFormModule { }
