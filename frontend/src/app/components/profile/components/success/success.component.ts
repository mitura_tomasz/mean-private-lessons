import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.scss']
})
export class SuccessComponent implements OnInit {

  option: string;

  constructor(private _activeRoute: ActivatedRoute) { }

  ngOnInit() {
    this.option = this.getOptionFromParams();
  }

  getOptionFromParams(): string {
    let option;
    this._activeRoute.paramMap.subscribe(params => {
      option = params.get('id');
    });
    return option;
  }

}
