import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from './../../../../../../services/auth.service';
import { PrivateLessonsService } from './../../../../../../services/private-lessons.service';

import { PrivateLesson } from './../../../../../../classes/private-lesson';


@Component({
  selector: 'app-my-announcement',
  templateUrl: './my-announcement.component.html',
  styleUrls: ['./my-announcement.component.scss']
})
export class MyAnnouncementComponent implements OnInit {

  @Input() privateLesson: PrivateLesson;
  @Output() $privateLessonEmitter: EventEmitter<string> = new EventEmitter;

  constructor(
    private _router: Router,
    private _authService: AuthService,
    private _privateLessonsService: PrivateLessonsService
  ) { }

  ngOnInit() {
  }

  onEditClick(id: string) {
    this._router.navigate(['profile/my-offers/edit', id]);

  }

  onPreviewClick(id: string) {
    this._router.navigate(['/announcements', id]);
  }

  onDeleteClick(id: string) {
    this._privateLessonsService.deletePrivateLessonByID(id).subscribe();
    this._authService.removeLesson(id);

    this.$privateLessonEmitter.emit(id);
  }


}
