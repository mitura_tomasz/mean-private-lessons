import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyAnnouncementComponent } from './my-announcement.component';

describe('MyAnnouncementComponent', () => {
  let component: MyAnnouncementComponent;
  let fixture: ComponentFixture<MyAnnouncementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyAnnouncementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyAnnouncementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
