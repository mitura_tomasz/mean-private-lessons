import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { OfferFormModule } from './../offer-form/offer-form.module';

import { MyOffersComponent } from './my-offers.component';
import { MyAnnouncementComponent } from './components/my-announcement/my-announcement.component';
import { EditOfferComponent } from './components/edit-offer/edit-offer.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    OfferFormModule
  ],
  declarations: [
    MyAnnouncementComponent,
    MyOffersComponent,
    EditOfferComponent,
  ],
  exports: [MyOffersComponent]
})
export class MyOffersModule { }
