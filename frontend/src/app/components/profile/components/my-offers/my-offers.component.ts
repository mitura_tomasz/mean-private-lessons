import { Component, OnInit } from '@angular/core';

import { AuthService } from './../../../../services/auth.service';
import { PrivateLessonsService } from './../../../../services/private-lessons.service';

import { PrivateLesson } from './../../../../classes/private-lesson';
import { User } from './../../../../classes/user';


@Component({
  selector: 'app-my-offers',
  templateUrl: './my-offers.component.html',
  styleUrls: ['./my-offers.component.scss']
})
export class MyOffersComponent implements OnInit {

  privateLessons: PrivateLesson[];
  isPrivateLessonsListEmpty: boolean = false;

  constructor(
    private _authService: AuthService,
    private _privateLessonsService: PrivateLessonsService
  ) { }

  ngOnInit() {

    this._authService.getProfile().subscribe(
      profile => {
        let myOffersId: string[] = profile.user.lessonsID;

        this.privateLessons = [];

        myOffersId.filter(offerID => {
          this._privateLessonsService.getPrivateLessonByID(offerID).then(
            privateLesson => {
              this.privateLessons.push(privateLesson);
            }
          );
        });

        if (!myOffersId.length) this.isPrivateLessonsListEmpty = true;
      },
      err => {
        console.log(err);
        return false;
      }
    );

  }

  handleDeleteClick(id) {
    this.privateLessons = this.privateLessons.filter((x: any) => x._id !== id);
    if (!this.privateLessons.length) this.isPrivateLessonsListEmpty = true;
  }


}
