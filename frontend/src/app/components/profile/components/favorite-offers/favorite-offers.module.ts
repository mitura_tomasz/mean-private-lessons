import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FavoriteOffersComponent } from './favorite-offers.component';
import { FavoriteAnnouncementComponent } from './components/favorite-announcement/favorite-announcement.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [FavoriteOffersComponent, FavoriteAnnouncementComponent],
  exports: [FavoriteOffersComponent]
})
export class FavoriteOffersModule { }
