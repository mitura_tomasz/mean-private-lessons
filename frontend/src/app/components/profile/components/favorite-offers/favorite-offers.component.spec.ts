import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavoriteOffersComponent } from './favorite-offers.component';

describe('FavoriteOffersComponent', () => {
  let component: FavoriteOffersComponent;
  let fixture: ComponentFixture<FavoriteOffersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavoriteOffersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavoriteOffersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
