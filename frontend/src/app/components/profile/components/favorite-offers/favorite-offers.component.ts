import { Component, OnInit } from '@angular/core';

import { AuthService } from './../../../../services/auth.service';
import { PrivateLessonsService } from './../../../../services/private-lessons.service';

import { PrivateLesson } from './../../../../classes/private-lesson';


@Component({
  selector: 'app-favorite-offers',
  templateUrl: './favorite-offers.component.html',
  styleUrls: ['./favorite-offers.component.scss']
})
export class FavoriteOffersComponent implements OnInit {

  favoritePrivateLessons: PrivateLesson[];
  isfavoritePrivateLessonsListEmpty: boolean = false;

  profile: any;


  constructor(
    private _authService: AuthService,
    private _privateLessonsService: PrivateLessonsService
  ) { }

  ngOnInit() {
    this._authService.getProfile().subscribe(
      resProfile => {
        this.profile = resProfile;
        let favoritePrivateLessonsID = resProfile.user.favoriteLessonsID;

        this.favoritePrivateLessons = [];

        favoritePrivateLessonsID.filter(offerID => {
          this._privateLessonsService.getPrivateLessonByID(offerID).then(
            privateLesson => {
              this.favoritePrivateLessons.push(privateLesson);
            }
          );
        });

        if (!favoritePrivateLessonsID.length) this.isfavoritePrivateLessonsListEmpty = true;

      }
    );
  }


  handleDeleteClick(id) {
    this.favoritePrivateLessons = this.favoritePrivateLessons.filter((x: any) => x._id !== id);
    if (!this.favoritePrivateLessons.length) this.isfavoritePrivateLessonsListEmpty = true;
  }

}
