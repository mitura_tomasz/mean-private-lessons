import { Component, OnInit, Input,  Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { PrivateLesson } from './../../../../../../classes/private-lesson';
import { AuthService } from './../../../../../../services/auth.service';


@Component({
  selector: 'app-favorite-announcement',
  templateUrl: './favorite-announcement.component.html',
  styleUrls: ['./favorite-announcement.component.scss']
})
export class FavoriteAnnouncementComponent implements OnInit {
  
  @Input() privateLesson: PrivateLesson;
  @Output() $privateLessonEmitter: EventEmitter<string> = new EventEmitter;

  
  constructor(
    private _router: Router,
    private _authService: AuthService,

  ) { }

  ngOnInit() {
  }
  
  onPreviewClick(id: string) {
    this._router.navigate(['/announcements', id]);
  }
  
  onDeleteClick(id: string) {
    this._authService.removeLessonFromFavorites(id);
    
    this.$privateLessonEmitter.emit(id);

  }
  
}
