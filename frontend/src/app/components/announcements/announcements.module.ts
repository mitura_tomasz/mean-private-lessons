import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { PrivateLessonsService } from './../../services/private-lessons.service';
import { CitiesService } from './../../services/cities.service';

import { TruncatePipe } from './../../pipes/truncate.pipe';

import { AnnouncementsRoutingModule } from './announcements-routing.module';
import { AnnouncementsComponent } from './announcements.component';
import { SearchParametrsComponent } from './components/search-parametrs/search-parametrs.component';
import { SearchOrderComponent } from './components/search-order/search-order.component';
import { SearchResultsComponent } from './components/search-results/search-results.component';
import { DetailedAnnouncementComponent } from './components/detailed-announcement/detailed-announcement.component';
import { ShortAnnouncementComponent } from './components/short-announcement/short-announcement.component';

@NgModule({
  imports: [
    CommonModule,
    AnnouncementsRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [
    AnnouncementsComponent,
    SearchParametrsComponent,
    SearchOrderComponent,
    SearchResultsComponent,
    DetailedAnnouncementComponent,
    ShortAnnouncementComponent,
    TruncatePipe
  ],
  providers: [
    PrivateLessonsService,
    CitiesService
  ]
})
export class AnnouncementsModule { }
