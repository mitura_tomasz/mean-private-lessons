import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AnnouncementsComponent } from './announcements.component';
import { SearchParametrsComponent } from './components/search-parametrs/search-parametrs.component';
import { SearchOrderComponent } from './components/search-order/search-order.component';
import { SearchResultsComponent } from './components/search-results/search-results.component';
import { DetailedAnnouncementComponent } from './components/detailed-announcement/detailed-announcement.component';

const routes: Routes = [
  {
    path: 'announcements',
    children: [
      { path: '', component: AnnouncementsComponent },
      { path: ':id', component: DetailedAnnouncementComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AnnouncementsRoutingModule { }
