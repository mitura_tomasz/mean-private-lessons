import { Component, OnInit, Input } from '@angular/core';

import { PrivateLesson } from './../../../../classes/private-lesson';


@Component({
  selector: 'app-short-announcement',
  templateUrl: './short-announcement.component.html',
  styleUrls: ['./short-announcement.component.scss'],
})
export class ShortAnnouncementComponent implements OnInit {
  
  @Input() privateLesson: PrivateLesson;
  
  constructor() { }

  ngOnInit() {
  }

}
