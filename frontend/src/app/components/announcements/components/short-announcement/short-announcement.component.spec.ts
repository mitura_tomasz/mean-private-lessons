import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShortAnnouncementComponent } from './short-announcement.component';

describe('ShortAnnouncementComponent', () => {
  let component: ShortAnnouncementComponent;
  let fixture: ComponentFixture<ShortAnnouncementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShortAnnouncementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShortAnnouncementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
