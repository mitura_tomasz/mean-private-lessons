import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailedAnnouncementComponent } from './detailed-announcement.component';

describe('DetailedAnnouncementComponent', () => {
  let component: DetailedAnnouncementComponent;
  let fixture: ComponentFixture<DetailedAnnouncementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailedAnnouncementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailedAnnouncementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
