import {
  Component,
  OnInit,
  trigger,
  state,
  style,
  transition,
  animate,
  keyframes
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import {
  FormControl,
  FormGroup,
  FormArray,
  FormBuilder,
  Validators,
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';
import { Headers } from '@angular/http';

import { PrivateLessonsService } from './../../../../services/private-lessons.service';
import { AuthService } from './../../../../services/auth.service';

import { User } from './../../../../classes/user';

import { PrivateLesson } from './../../../../classes/private-lesson';


@Component({
  selector: 'app-detailed-announcement',
  templateUrl: './detailed-announcement.component.html',
  styleUrls: ['./detailed-announcement.component.scss'],
  animations: [
    trigger('crossfade', [
      state('show', style({ opacity: 1 })),
      state('hide', style({ opacity: 0 })),
      transition('* => show', animate('1s ease-in')),
      transition('show => hide', animate('1s ease-out'))
    ]),
    trigger('crossfade-container', [
      state('neutral', style({ 'background-color': '#325D88' })),
      state('success', style({ 'background-color': '#93C54B', 'transition': '1s' })),
    ])
  ],
})
export class DetailedAnnouncementComponent implements OnInit {

  privateLesson: PrivateLesson;
  privateLessonID: number;
  privateLessonOwnerProfile: User;
  profile: User;
  emailForm: FormGroup;
  isEmailSended: boolean = false;
  isMyFavoriteAnnouncement: boolean;

  stateNeutral = 'show';
  stateSuccess = 'hide';
  crossfadeContainerState = 'neutral';

  constructor(
    private _privateLessonsService: PrivateLessonsService,
    private _activatedRoute: ActivatedRoute,
    private _authService: AuthService,
    private _httpClient: HttpClient,
    private _formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.emailForm = this._formBuilder.group({
      'message': [
        {
          value: null,
          disabled: false
        },
        Validators.compose([
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(255),
        ])
      ],
      'sender-email-address': [
        '',
        [
          Validators.required,
          this.emailValidator
        ],
      ],
      'reveiver-email-address': []
    });

    this._activatedRoute.params.subscribe(params => {
      this.privateLessonID = params['id'];
    });

    this._privateLessonsService.getPrivateLessons().subscribe(
      responsePrivateLessons => {
        this.privateLesson = responsePrivateLessons.find(x => x._id === this.privateLessonID);

        this._authService.getProfileByID(this.privateLesson.ownerID).subscribe(
          responseProfile => {
            this.privateLessonOwnerProfile = responseProfile;
          }
        );

      }
    );

    if (this._authService.loggedIn()) {
      this._authService.getProfile().subscribe(
        responseProfile => {
          this.profile = responseProfile.user;
          this.emailForm.controls['sender-email-address'].setValue(this.profile.email);

          let favoriteLessonsID = [];
          this.profile.favoriteLessonsID.forEach(x => {
            let tmp = x;
            favoriteLessonsID.push(tmp);
          });

          this.isMyFavoriteAnnouncement = favoriteLessonsID.includes(this.privateLessonID) ? true : false;
        }
      );
    }

  }

  emailValidator(control) {
    if (control.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
      return null;
    } else {
      return { 'invalidEmailAddress': true };
    }
  }

  switchTitles() {
    if (this.stateNeutral === 'show') {
      this.stateSuccess = 'show';
      this.stateNeutral = 'hide';
    }
    this.crossfadeContainerState = 'success';
  }

  sendMail(email: EmailInterface) {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    this._httpClient.post('http://localhost:3000/send', email, headers)
      .map((res: Response) => res.json())
      .subscribe(
        (data: any) => console.log(data),
        (error: HttpErrorResponse) =>  this._authService.handleErrorResponse(error)
      );
  }

  onEmailSubmit(emailForm: any) {
    if (this.emailForm.valid && !this.isEmailSended) {

      let email: EmailInterface = {
        message: this.emailForm.controls['message'].value,
        senderEmailAddress: typeof this.profile === 'object' ? this.profile.email : this.emailForm.controls['sender-email-address'].value,
        senderUsername: typeof this.profile === 'object' ? this.profile.username : 'niezarejestrowanego',
        receiverEmailAddress: this.privateLessonOwnerProfile.email
      };

      this.switchTitles();
      this.sendMail(email);

      (<FormControl>this.emailForm.controls['message']).disable();
      (<FormControl>this.emailForm.controls['sender-email-address']).disable();
      this.isEmailSended = true;

      console.log("Form Submitted!");
    } else {
      console.log("Form not Submitted! Something went wrong!");
    }
  }

  onFavoriteClick() {
    if (!this.isMyFavoriteAnnouncement) {
      this._authService.addLessonToFavorites(this.privateLessonID);
      this.isMyFavoriteAnnouncement = true;
    }
  }
}


export interface EmailInterface {
  message: string;
  senderEmailAddress: string;
  receiverEmailAddress: string;
  senderUsername: string;
}
