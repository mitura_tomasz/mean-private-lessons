import { Component, OnInit } from '@angular/core';

import { PrivateLessonsService } from './../../../../services/private-lessons.service';

import { PrivateLesson } from './../../../../classes/private-lesson';



@Component({
  selector: 'app-search-order',
  templateUrl: './search-order.component.html',
  styleUrls: ['./search-order.component.scss']
})
export class SearchOrderComponent implements OnInit {

  privateLessons: PrivateLesson[];

  constructor(private _privateLessonsService: PrivateLessonsService) { }

  ngOnInit() {
    this.privateLessons = this._privateLessonsService.getFilteredPrivateLessons();

    this._privateLessonsService.filteredPrivateLessonsUpdate.subscribe(
      () => this.privateLessons = this._privateLessonsService.getFilteredPrivateLessons()
    );
  }
}
