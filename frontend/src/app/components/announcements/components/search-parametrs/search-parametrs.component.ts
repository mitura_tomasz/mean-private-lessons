import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormArray,
  FormBuilder,
  Validators,
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';
import { PrivateLesson } from './../../../../classes/private-lesson';
import { LessonsService } from './../../../../services/lessons.service';
import { PrivateLessonsService } from './../../../../services/private-lessons.service';
import { CitiesService } from './../../../../services/cities.service';


@Component({
  selector: 'app-search-parametrs',
  templateUrl: './search-parametrs.component.html',
  styleUrls: ['./search-parametrs.component.scss']
})
export class SearchParametrsComponent implements OnInit {

  searchForm: FormGroup;
  privateLessons: PrivateLesson[];
  lessonSubjects: string[];
  selectedPrivateLessons: PrivateLesson[];
  voivodeships: string[];
  towns: string[];

  constructor(
    private _formBuilder: FormBuilder,
    private _privateLessonsService: PrivateLessonsService,
    private _lessonsService: LessonsService,
    private _citiesService: CitiesService
  ) { }

  ngOnInit() {

    this.towns = this._citiesService.getAllTowns();

    this._privateLessonsService.getPrivateLessons().subscribe(
      responseData => this.privateLessons = responseData
    );

    this.lessonSubjects = this._lessonsService.getLessonSubjects();
    this.voivodeships = this._citiesService.getVoivodeships();

    this.searchForm = this._formBuilder.group({
      'town': [
        null,
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(50)
        ])
      ],
      'subject': null,
      'level': null,
      'priceMin':
        [
          null,
          Validators.compose([
            Validators.min(0),
            Validators.max(1000)
          ])
        ]
      ,
      'priceMax':
        [
          null,
          Validators.compose([
            Validators.min(0),
            Validators.max(1000)
          ])
        ]
    });

  }


  onSearchSubmit(searchForm: any) {
    var filters = this.searchFormTofiltersArray(searchForm);
    let searchResults = this._privateLessonsService.getSearchResults(filters, searchForm);

    this._privateLessonsService.setFilteredPrivateLessons(searchResults);
  }

  searchFormTofiltersArray(searchForm: any) {
    let filters = [];

    if (searchForm.town !== null && searchForm.town !== '') { filters.push('town'); }
    if (searchForm.level !== null) { filters.push('level'); }
    if (searchForm.subject !== null) { filters.push('subject'); }

    if (searchForm.priceMin !== null && searchForm.priceMax !== null) {
      filters.push('priceMinAndMax');
    } else {
      if (searchForm.priceMin !== null) { filters.push('priceMin'); }
      if (searchForm.priceMax !== null) { filters.push('priceMax'); }
    }
    return filters;
  }



  onVoivodeshipClick(voivodeship: string) {
    let cities = this._citiesService.getCitiesByVoivodeship(voivodeship);

    let searchResults = this.privateLessons.filter(
      x => cities.some(y => y === x.town)
    );

    this._privateLessonsService.setFilteredPrivateLessons(searchResults);
  }

  onSelectedSubjectChange(subject) {
    (<FormControl>this.searchForm.controls['subject']).setValue(subject);
  }


}









