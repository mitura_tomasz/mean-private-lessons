import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchParametrsComponent } from './search-parametrs.component';

describe('SearchParametrsComponent', () => {
  let component: SearchParametrsComponent;
  let fixture: ComponentFixture<SearchParametrsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchParametrsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchParametrsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
