import { Component, OnInit } from '@angular/core';

import { PrivateLessonsService } from './../../../../services/private-lessons.service';

import { PrivateLesson } from './../../../../classes/private-lesson';


@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.scss']
})
export class SearchResultsComponent implements OnInit {

  privateLessons: PrivateLesson[];

  constructor(private _privateLessonsService: PrivateLessonsService) { }

  ngOnInit() {

    this.privateLessons = this._privateLessonsService.getFilteredPrivateLessons();

    this._privateLessonsService.filteredPrivateLessonsUpdate.subscribe(
      () => {
        this.privateLessons = this._privateLessonsService.getFilteredPrivateLessons();
      }
    );

  }

}
