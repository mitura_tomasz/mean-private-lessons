import { Injectable } from '@angular/core';

import { Cities } from '../classes/cities';

@Injectable()
export class CitiesService {

  cities: Cities[] = [
    new Cities('Dolnośląskie', ['Wrocław', 'Wałbrzych', 'Legnica', 'Jelenia Góra', 'lubin', 'Głogów', 'Świdnica', 'Bolesławiec', 'Oleśnica', 'Oława', 'Zgorzelec']),
    new Cities('Kujawsko-pomorskie', ['Bydgoszcz', 'Toruń', 'Włocławek', 'Grudziądz', 'Inowrocław', 'Brodnica', 'Świecie', 'Chełmno', 'Rypin', 'Solec', 'Chełmża', 'Lipno', 'Żnin', 'Wąbrzeźno', 'Tuchola']),
    new Cities('Lubelskie', ['Lublin', 'Zamość', 'Chełm', 'Biała', 'Puławy', 'Świdnik', 'Kraśnik', 'Łuków', 'Biłgoraj', 'Lubartów', 'Tomaszów', 'Łęczna']),
    new Cities('Lubuskie', ['Zielona Góra', 'Gorzów Wielkoski', 'Nowa Sól', 'Żary', 'Żagań', 'Świebodzin', 'Międzyrzecz', 'Sulechów', 'Słubice', 'Gubin', 'Lubsko']),
    new Cities('Mazowieckie', ["Warszawa", "Radom", "Płock", "Siedlce", 'Pruszków', "Legionowo", "Ostrołęka", "Piaseczno", "Otwock", "Wołomin", "Jabłonna"]),
    new Cities('Małopolskie', ['Kraków', 'Zakopane', 'Tarnów', 'Bukowno', 'Nowy Sącz', 'Nowy Targ', 'Chrzanów', 'Rabka-Zdrój', 'Libiąż', 'Szczawnica', 'Trzebinia']),
    new Cities('Opolskie', ["Opole", "Kędzierzyn", "Nysa", "Brzeg", "Kluczbork", "Prudnik", "Strzelce", "Krapkowice", "Namysłów", "Głuchołazy", "Głubczyce", "Zdzieszowice"]),
    new Cities('Podkarpackie', ["Rzeszów", "Przemyśl", "Stalowa", "Mielec", "Tarnobrzeg", "Krosno", "Dębica", "Jarosław", "Sanok", "Jasło", "Łańcut", "Przeworsk", "Ropczyce"]),
    new Cities('Podlaskie', ["Białystok", "Suwałki", "Łomża", "Augustów", "Bielsk", "Zambrów", "Grajewo", "Hajnówka", "Sokółka", "Łapy", "Siemiatycze", "Wasilków", "Kolno", "Mońki"]),
    new Cities('Pomorskie', ["Gdańsk", "Gdynia", "Słupsk", "Tczew", "Wejherowo", "Rumia", "Starogard", "Chojnice", "Malbork", "Kwidzyn", "Sopot", "Lębork", "Pruszcz", "Reda", "Kościerzyna", "Bytów"]),
    new Cities('Warmińsko-mazurskie', ["Olsztyn", "Elbląg", "Ełk", "Ostróda", "Iława", "Giżycko", "Kętrzyn", "Bartoszyce", "Szczytno", "Mrągowo", "Działdowo", "Pisz", "Braniewo", "Olecko"]),
    new Cities('Wielkopolskie', ["Poznań", "Kalisz", "Konin", "Piła", "Ostrów", "Gniezno", "Leszno", "Swarzędz", "Luboń", "Śrem", "Września", "Krotoszyn", "Turek", "Jarocin"]),
    new Cities('Zachodniopomorskie', ["Szczecin", "Koszalin", "Stargard", "Kołobrzeg", "Świnoujście", "Szczecinek", "Police", "Wałcz", "Białogard", "Goleniów", "Gryfino", "Nowogard"]),
    new Cities('Łódzkie', ["Łódź", "Piotrków Trybunalski", "Pabianice", "Tomaszów Mazowiecki", "Bełchatów", "Zgierz", "Skierniewice", "Radomsko", "Kutno", "ZduńskaWola", "Sieradz", "Łowicz", "Wieluń"]),
    new Cities('Śląskie', ["Katowice", "Częstochowa", "Sosnowiec", "Gliwice", "Zabrze", "Bielsko-Biała", "Bytom", "RudaŚląska", "Rybnik", "Tychy", "Dąbrowa Górnicza", "Chorzów", "Jaworzno", "Mysłowice"]),
    new Cities('Świętokrzyskie', ["Kielce", "Ostrowiec", "Starachowice", "Skarzysko", "Sandomierz", "Konskie", "Busko-Zdrój", "Gmina", "Staszow", "Pińczów", "gmina", "Suchedniów", "Połaniec"]),
  ];

  constructor() { }


  getCities() {
    return this.cities;
  }

  getVoivodeships() {
    let voivodeships = [];

    this.cities.filter(x => {
      voivodeships.push(x.voivodeship)
    });

    return voivodeships;
  }

  getCitiesByVoivodeship(voivodeship: string) {
    let cities = [];

    this.cities.filter(x => {
      if (x.voivodeship === voivodeship) { cities = x.towns; }
    });
    return cities.sort();
  }

  getAllTowns() {
    let towns = [];
    this.cities.filter(x => {
      x.towns.filter(y => {
        towns.push(y);
      });
    });
    return towns.sort();

  }

}
