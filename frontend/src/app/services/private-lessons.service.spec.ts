import { TestBed, inject } from '@angular/core/testing';

import { PrivateLessonsService } from './private-lessons.service';

describe('PrivateLessonsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PrivateLessonsService]
    });
  });

  it('should be created', inject([PrivateLessonsService], (service: PrivateLessonsService) => {
    expect(service).toBeTruthy();
  }));
});
