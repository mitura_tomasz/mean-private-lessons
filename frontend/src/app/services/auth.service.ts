import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from "rxjs/Observable";
import { tokenNotExpired } from 'angular2-jwt';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { map } from "rxjs/operator/map";


@Injectable()
export class AuthService {

  authToken: any;
  user: any;

  constructor(private _http: Http, private _httpClient: HttpClient) { }

  handleErrors(error: any) {
    if (error.status === 500) {
      return Observable.throw(new Error(error.status));
    }
    else if (error.status === 400) {
      return Observable.throw(new Error(error.status));
    }
    else if (error.status === 409) {
      return Observable.throw(new Error(error.status));
    }
    else if (error.status === 406) {
      return Observable.throw(new Error(error.status));
    }
  }

  handleErrorResponse(error: HttpErrorResponse) {
    if (error.error instanceof Error) {
      console.log("Client-side error occured.");
    } else {
      console.log("Server-side error occured.");
    }
  }


  registerUser(user) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this._http.post('http://localhost:3000/users/register', user, { headers: headers })
      .map(res => res.json())
      .catch((error: any) => this.handleErrors(error));
    ;
  }

  authenticateUser(user) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this._http.post('http://localhost:3000/users/authenticate', user, { headers: headers })
      .map(res => res.json())
      .catch((error: any) => this.handleErrors(error));
  }

  storeUserData(token, user) {
    localStorage.setItem('id_token', token);
    localStorage.setItem('user', JSON.stringify(user));
    this.authToken = token;
    this.user = user;
  }

  getProfile() {
    let headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken);
    headers.append('Content-Type', 'application/json');
    return this._http.get('http://localhost:3000/users/profile', { headers: headers })
      .map(res => res.json())
      .catch((error: any) => this.handleErrors(error));
  }

  getProfileByID(profileID) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this._http.get('http://localhost:3000/users/profile/' + profileID, { headers: headers })
      .map(res => res.json())
      .catch((error: any) => this.handleErrors(error));
  }

  loadToken() {
    const token = localStorage.getItem('id_token');
    this.authToken = token;
  }

  loggedIn() {
    return tokenNotExpired('id_token');
  }

  logout() {
    this.authToken = null;
    this.user = null;
    localStorage.clear();
  }

  addLesson(privateLessonID: string) {
    this.user = this.getProfile().subscribe(
      profile => {
        this.user = profile.user;
        this.user.lessonsID.push(privateLessonID);
        let path = 'http://localhost:3000/users/removeLessonID/' + JSON.parse(localStorage.getItem('user')).id;

        this._httpClient.put(path, this.user)
          .subscribe(
            (data: any) => console.log(data),
            (error: HttpErrorResponse) => this.handleErrorResponse(error)
          );

      },
      error => {
        console.log(error);
        return false;
      }
    );
  }

  removeLesson(id: string) {
    this.user = this.getProfile().subscribe(
      profile => {
        this.user = profile.user;
        this.user.lessonsID = this.user.lessonsID.filter(val => val !== id);
        let path = 'http://localhost:3000/users/removeLessonID/' + JSON.parse(localStorage.getItem('user')).id;

        this._httpClient.put(path, this.user)
          .subscribe(
            (data: any) => console.log(data),
            (error: HttpErrorResponse) => this.handleErrorResponse(error)
          );

      },
      error => {
        console.log(error);
        return false;
      }
    );
  }

  addLessonToFavorites(lessonID) {
    this.user = this.getProfile().subscribe(
      profile => {
        this.user = profile.user;
        this.user.favoriteLessonsID.push(lessonID);
        let path = 'http://localhost:3000/users/removeLessonID/' + JSON.parse(localStorage.getItem('user')).id;

        this._httpClient.put(path, this.user)
          .subscribe(
            (data: any) => console.log(data),
            (error: HttpErrorResponse) => this.handleErrorResponse(error)
          );
      },
      error => {
        console.log(error);
        return false;
      }
    );
  }

  removeLessonFromFavorites(lessonID) {
    this.user = this.getProfile().subscribe(
      profile => {
        this.user = profile.user;
        this.user.favoriteLessonsID = this.user.favoriteLessonsID.filter(x => x !== lessonID);
        let path = 'http://localhost:3000/users/removeLessonID/' + JSON.parse(localStorage.getItem('user')).id;

        this._httpClient.put(path, this.user)
          .subscribe(
            (data: any) => console.log(data),
            (error: HttpErrorResponse) => this.handleErrorResponse(error)
          );
      },
      error => {
        console.log(error);
        return false;
      }
    );
  }

}
