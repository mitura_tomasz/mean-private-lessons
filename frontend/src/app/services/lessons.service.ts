import { Injectable } from '@angular/core';

import { Lessons } from '../classes/lessons';


@Injectable()
export class LessonsService {

  lessons: Lessons[] = [
    new Lessons('Języki obce', ['Język angielski', 'Język niemiecki', 'Język francuski', 'Język hiszpański', 'Język rosysjki', 'Język włoski']),
    new Lessons('Ścisłe', ['Matematyka', 'Informatyka', 'Statystyka', 'Architektura', 'Programowanie', 'Analiza Matematyczna']),
    new Lessons('Przyrodnicze', ['Biologia', 'Fizyka', 'Geografia', 'Chemia', 'Biologia molekularna', 'Astronomia']),
    new Lessons('Humanistyczne/społeczne', ['Język polski', 'Wos', 'Historia', 'Pedagogika', 'Nauczanie początkowe', 'Historia sztuki']),
    new Lessons('Ekonomiczne', ['Ekonomia', 'Rachunkowość', 'Ekonometria', 'Marketing', 'Logistyka', 'Finanse'])
  ];

  constructor() { }


  getLessons() {
    return this.lessons
  }

  getLessonSubjects() {
    let lessonArrays = this.lessons.map(x => {
      return x.subjects;
    });

    var concatedArrays = [];
    lessonArrays.forEach(x => {
      concatedArrays = concatedArrays.concat(x);
    });

    return concatedArrays.sort();
  }

  getLessonsList() {
    let lessons = [];

    this.lessons.forEach(x => {
      x.subjects.forEach(y => {
        lessons.push(y);
      });
    });

    return lessons.sort();
  }

}
