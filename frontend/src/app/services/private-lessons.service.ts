import { Injectable, EventEmitter } from '@angular/core'
import { Observable } from "rxjs/Observable";
import { Http, Response } from '@angular/http';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

import { PrivateLesson } from './../classes/private-lesson';


@Injectable()
export class PrivateLessonsService {

  private _privateLessonsUrl: string = "http://localhost:3000/api/lessons";
  public allPrivateLessons: PrivateLesson[];
  public filteredPrivateLessons: PrivateLesson[];
  public filteredPrivateLessonsUpdate = new EventEmitter<PrivateLesson[]>();


  constructor(private _http: Http, private _httpClient: HttpClient) {
    this.getPrivateLessons().subscribe(response => {
      this.filteredPrivateLessons = response;
      this.allPrivateLessons = response;
    });
  }


  handleErrors(error: any) {
    if (error.status === 500) {
      return Observable.throw(new Error(error.status));
    }
    else if (error.status === 400) {
      return Observable.throw(new Error(error.status));
    }
    else if (error.status === 409) {
      return Observable.throw(new Error(error.status));
    }
    else if (error.status === 406) {
      return Observable.throw(new Error(error.status));
    }
  }

  getPrivateLessons() {
    return this._http.get(this._privateLessonsUrl)
      .map((response: Response) => response.json())
      .catch((error: any) => this.handleErrors(error));
  }

  getPrivateLessonByID(lessonID: string) {
    return this._http.get(this._privateLessonsUrl + '/' + lessonID)
      .map((response: Response) => response.json())
      .catch((error: any) => this.handleErrors(error))
      .toPromise();
  }

  deletePrivateLessonByID(lessonID: string) {
    const url = `${this._privateLessonsUrl}/${lessonID}`;

    return this._http.delete(url)
      .map((response: Response) => response.json())
      .catch((error: any) => this.handleErrors(error));
  }

  updatePrivateLesson(privateLesson: PrivateLesson, lessonID: string) {
    const url = `${this._privateLessonsUrl}/${lessonID}`;

    return this._http.put(url, privateLesson)
      .map((response: Response) => response.json())
      .catch((error: any) => this.handleErrors(error));
  }

  getFilteredPrivateLessons() {
    return this.filteredPrivateLessons;
  }

  setFilteredPrivateLessons(privateLessons: PrivateLesson[]) {
    this.filteredPrivateLessons = privateLessons;
    this.filteredPrivateLessonsUpdate.emit(this.filteredPrivateLessons);
  }

  isPrivateLessonAcceptConditions(privateLesson: PrivateLesson, filters: string[], searchForm: any) {

    let isPrivateLessonValid = true;
    for (let i = 0, maxI = filters.length; i < maxI; i++) {
      switch (filters[i]) {
        case "searchTerm": privateLesson.title.toUpperCase().includes(searchForm.searchTerm.toUpperCase()) ? isPrivateLessonValid = true : isPrivateLessonValid = false; break;
        case "town": searchForm.town.toUpperCase() === privateLesson.town.toUpperCase() ? isPrivateLessonValid = true : isPrivateLessonValid = false; break;
        case "subject": privateLesson.subject === searchForm.subject ? isPrivateLessonValid = true : isPrivateLessonValid = false; break;
        case "level": privateLesson.levels.some(x => x === searchForm.level) ? isPrivateLessonValid = true : isPrivateLessonValid = false; break;
        case "priceMinAndMax": searchForm.priceMin <= privateLesson.pricePerHour && searchForm.priceMax >= privateLesson.pricePerHour ? isPrivateLessonValid = true : isPrivateLessonValid = false; break;
        case "priceMin": searchForm.priceMin <= privateLesson.pricePerHour ? isPrivateLessonValid = true : isPrivateLessonValid = false; break;
        case "priceMax": searchForm.priceMax >= privateLesson.pricePerHour ? isPrivateLessonValid = true : isPrivateLessonValid = false; break;
      }
      if (isPrivateLessonValid === false) break;
    }
    return isPrivateLessonValid;
  }

  getSearchResults(filters: any, searchForm: any) {
    let searchResults = this.allPrivateLessons.filter(
      x => this.isPrivateLessonAcceptConditions(x, filters, searchForm)
    );
    return searchResults;
  }

}



