import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ProfileModule } from './components/profile/profile.module';
import { AnnouncementsModule } from './components/announcements/announcements.module';
import { AppRoutingModule } from './app-routing.module';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ValidateService } from './services/validate.service';
import { AuthService } from './services/auth.service';
import { LessonsService } from './services/lessons.service';

import { AuthGuard } from './guards/auth.guard';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { RegisterComponent } from './components/register/register.component';
import { FooterComponent } from './components/footer/footer.component';
import { LessonsComponent } from './components/home/lessons/lessons.component';
import { CitiesComponent } from './components/home/cities/cities.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    HomeComponent,
    RegisterComponent,
    FooterComponent,
    LessonsComponent,
    CitiesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    FlashMessagesModule,
    HttpModule,
    NgbModule.forRoot(),
    ReactiveFormsModule,
    ProfileModule,
    AnnouncementsModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [
    ValidateService,
    AuthService,
    AuthGuard,
    LessonsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
