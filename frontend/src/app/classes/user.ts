import { PrivateLesson } from './private-lesson';

export class User {
    
    constructor(
        public name: string, 
		public email: string,
		public phone: number,
		public username: string,
		public password: string,
        public lessonsID: PrivateLesson[], 
        public favoriteLessonsID: PrivateLesson[], 
    ) { }

    
}
