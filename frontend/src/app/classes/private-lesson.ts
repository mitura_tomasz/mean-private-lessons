export class PrivateLesson {
    
    constructor(
        public title: String, 
        public subject: String, 
		public pricePerHour: Number,
        public town: String, 
        public description: String, 
        public placeOfLesson: String, 
        public levels: String[], 
        public ownerID: String
    ) { }

    
}
